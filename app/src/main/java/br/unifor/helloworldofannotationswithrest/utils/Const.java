package br.unifor.helloworldofannotationswithrest.utils;

/**
 * Created by marinagosson on 26/05/15.
 */

public class Const {

    /**
     * Acoes que instaciam os dialogs
     */
    public final static String START_PROGRESS_REQUEST_ACTION = "START_PROGRESS_REQUEST_ACTION";
    public final static String STOP_PROGRESS_REQUEST_ACTION = "STOP_PROGRESS_REQUEST_ACTION";
    public final static String UNAUTHORIZED_REQUEST_ACTION = "UNAUTHORIZED_REQUEST_ACTION";
    public final static String USER_PASSWORD_INVALID_REQUEST_ACTION = "USER_PASSWORD_INVALID_REQUEST_ACTION";
    public final static String EMAIL_INVALIDO = "EMAIL_INVALIDO";
    public final static String EMAIL_EXISTENTE_REQUEST_ACTION = "EMAIL_EXISTENTE_REQUEST_ACTION";
    public final static String CONFLIT = "CONFLIT";
    public final static String USUARIO_EXISTENTE_REQUEST_ACTION = "USUARIO_EXISTENTE_REQUEST_ACTION";
    public final static String OTHERS_ERROS = "OTHERS_ERROS";

    public final static String UPDATE_LISTVIEW_SOLICITACOES_PENDENTES = "UPDATE_LISTVIEW_SOLICITACOES_PENDENTES";
    public final static String UPDATE_LISTVIEW_NOTIFICACAO_AGENDAMENTO = "UPDATE_LISTVIEW_NOTIFICACAO_AGENDAMENTO";
    public final static String UPDATE_LISTVIEW_NOTIFICACAO_QUARENTENA = "UPDATE_LISTVIEW_NOTIFICACAO_QUARENTENA";

    public final static String UPDATE_LISTVIEW = "UPDATE_LISTVIEW";
    public final static String NEW_NOTIFICATION = "NEW_NOTIFICATION";

    public static final String ERROR_SERVICO = "ERROR_SERVICO";


    /**
     * Fonte do aplicativo
     */
    public final static String TYPEFACE = ".ttf";


}
