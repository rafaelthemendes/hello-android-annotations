package br.unifor.helloworldofannotationswithrest.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.rest.config.RestPath;

public class Utils {

    private static final String TAG = "Utils";

    private static final boolean DEBUG_APPLICATION = true;

    public static final String SIMPLE_DATE_PATTERN = "dd/MM/yyyy";
    //    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DATE_TIME_PATTERN = "yyyy-mm-dd'T'HH:MM:SS.SSS";

    /**
     * Cria uma instancia do Gson usando a serializacao/deserializacao de Date
     * customizada pela classe JsonDate
     *
     * @return instancia do Gson
     */
    public static Gson getGsonInstance() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDate());
        return gsonBuilder.create();
    }

    /**
     * Formata a data para string.
     *
     * @param date
     * @return Utils.DATE_TIME_PATTERN formatted date
     */
    public static String getStringFromDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_PATTERN,
                Locale.getDefault());
        return sdf.format(date);

    }

    /**
     * Formata a data para string no formato Utils.DATE_TIME_PATTERN.
     *
     * @param date
     * @return Utils.DATE_TIME_PATTERN formatted date
     */
    public static String getDateTimeStringFromDate(Date date) {
        SimpleDateFormat formatUTC = new SimpleDateFormat(DATE_TIME_PATTERN);

        return formatUTC.format(date);
    }

    /**
     * Recupera a data a partir de uma string no formato
     * Utils.DATE_TIME_PATTERN.
     *
     * @param utcDate
     * @return Date
     */
    public static Date getDateFromDateTimeString(String utcDate) {

        DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
        DateTime dateTime = dtf.parseDateTime(utcDate);

        return dateTime.toDate();

    }

    /**
     * Alerta o usuário com um dialogo de OK com a mensagem informada
     *
     * @param message
     * @param context
     */
    public static void alertUser(String message, Context context) {
        alertUser(message, context, null);
    }

    private static AlertDialog.Builder buildDefaultAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        return builder;
    }

    /**
     * Alerta o usuário com um dialogo de OK com a mensagem informada
     *
     * @param message
     * @param context
     * @param onClickListener
     */
    public static void alertUser(String message, Context context,
                                 OnClickListener onClickListener) {

        AlertDialog.Builder builder = buildDefaultAlert(context);

        builder.setMessage(message);
        builder.setPositiveButton("OK", onClickListener);

        builder.create().show();
    }

    public static void alertUser(String message, Context context,
                                 OnClickListener positiveListener, OnClickListener negativeListener) {

        AlertDialog.Builder builder = buildDefaultAlert(context);

        builder.setMessage(message);
        builder.setPositiveButton("Sim", positiveListener);
        builder.setNegativeButton("Não", negativeListener);

        builder.create().show();

    }

    /**
     * Verifica conexão e mostra um alert para o usuario caso ela nao exista
     *
     * @param context
     * @return network Availability
     */
    public static boolean checkConnection(Context context) {

        boolean networkAvailability = isNetworkAvailable(context);

        if (!networkAvailability) {
            alertUser("Você não está conectado a internet. Conecte-se para utilizar o aplicativo",
                    context);
        }

        return networkAvailability;

    }

    /**
     * Verifica se existe uma conexao com a internet disponivel
     *
     * @param context
     * @return true se existe, false o contrario
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Bitmap getBitmapFromURL(String link) {

        try {
            URL url = new URL(link);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static ShapeDrawable createCircleColorCar(int color) {
        ShapeDrawable circle = new ShapeDrawable(new OvalShape());
        circle.getPaint().setColor(color);

        return circle;
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    public static boolean checkPlayServices(Context context, Activity act) {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, act, 9000).show();
            } else {
                Log.i("Utils.checkPlayServices", "This device is not supported.");
            }
            return false;
        }

        Log.i("Utils.checkPlayServices", "Tudo Ok.");

        return true;
    }

}
