package br.unifor.helloworldofannotationswithrest.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

public class JsonDate implements JsonDeserializer<Date>, JsonSerializer<Date> {
	@Override
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		Date date = Utils.getDateFromDateTimeString(json.getAsString());
		return date;
	}
	@Override
	public JsonElement serialize(Date date, Type typeOfT, JsonSerializationContext context) {
		String utc = Utils.getDateTimeStringFromDate(date);
		return new JsonPrimitive(utc);
	}
}