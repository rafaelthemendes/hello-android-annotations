package br.unifor.helloworldofannotationswithrest.controller;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.rest.RestService;

import java.sql.SQLException;
import java.util.List;

import br.unifor.helloworldofannotationswithrest.database.DatabaseHelper;
import br.unifor.helloworldofannotationswithrest.model.Review;
import br.unifor.helloworldofannotationswithrest.model.User;
//import br.unifor.helloworldofannotationswithrest.preferences.HelloAnnotationsPreferences_;
import br.unifor.helloworldofannotationswithrest.rest.UserRest;
import br.unifor.helloworldofannotationswithrest.rest.config.RequestErrorHandler;
import br.unifor.helloworldofannotationswithrest.rest.config.RestConfig;
import br.unifor.helloworldofannotationswithrest.rest.config.RestParam;
import br.unifor.helloworldofannotationswithrest.rest.entityRest.Login;
import br.unifor.helloworldofannotationswithrest.rest.entityRest.Register;

@EBean
public class UserController {

    private final String TAG = "UserController";

    @OrmLiteDao(helper = DatabaseHelper.class, model = User.class)
    Dao<User, Integer> userDao;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Review.class)
    Dao<Review, Integer> reviewDao;

    @Bean
    RequestErrorHandler requestErrorHandler;

    @RestService
    UserRest userRest;


    @AfterInject
    public void afterInject() {

        if (getUsuarioLogado() != null) {
            String token = getUsuarioLogado().getToken();
            userRest.setHeader(RestParam.PARAM_NAME_AUTHORIZATION, RestParam.PARAM_NAME_TOKEN + " " + token);
        }
        userRest.setRestErrorHandler(requestErrorHandler);
    }


    /**
     * Metodo para realizar o login
     * Apos receber o usuario, salva no banco de dados
     *
     * @param email
     * @param password
     */
    public void requestLogin(String email, String password) throws Exception {
        Login login = new Login(email, password);
        User userLoggedIn = userRest.login(login);

        if(userLoggedIn != null) {
            persistUser(userLoggedIn);
            if (RestConfig.DEBUG)
                Log.i(TAG, "Request de Login realizado com Sucesso.");
        } else throw new Exception();

    }

    public void persistUser(User usuario) {
        try {
            userDao.createOrUpdate(usuario);
            if (RestConfig.DEBUG)
                Log.i(TAG, "User salvo no banco ... " + usuario.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getUsuarioLogado() {

        User usuario = null;

        try {
            if (userDao.queryForAll().size() != 0) {
                usuario = userDao.queryForAll().get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return usuario;
    }

    public void register(String username, String email, String password) throws Exception {

        Register register = new Register();
        register.setUsername(username);
        register.setEmail(email);
        register.setPassword(password);

        User user = userRest.register(register);

        if (user != null) {
            persistUser(user);
            if (RestConfig.DEBUG)
                Log.i(TAG, "Request de Cadastro realizado com Sucesso.");
        } else throw new Exception();
    }


    public void requestReviews() throws Exception {
        List<Review> reviews = userRest.getFeed();
        if (reviews.size() > 0)
            persistReviews(reviews);
    }

    public void persistReviews(List<Review> reviews) {
        try {
            for (Review review : reviews) {
                reviewDao.createOrUpdate(review);
                Log.i(TAG, "ReviewEntity salva: " + review.getComment());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Review> getReviews(){
        List<Review> reviews = null;
        try {
            reviews = reviewDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reviews;
    }



}
