package br.unifor.helloworldofannotationswithrest.rest.config;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class BaseInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] data, ClientHttpRequestExecution execution) throws IOException {

        if (RestConfig.DEBUG) {
            RestUtils.debugRequest(BaseInterceptor.class.getName(), request);
        }

        return execution.execute(request, data);
    }

}
