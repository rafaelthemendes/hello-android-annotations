package br.unifor.helloworldofannotationswithrest.rest.config;

public class RestParam {
    public static final String PARAM_NAME_TOKEN = "Token";
    public static final String PARAM_NAME_AUTHORIZATION = "Authorization";
}
