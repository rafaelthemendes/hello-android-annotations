package br.unifor.helloworldofannotationswithrest.rest.config;

import android.content.Context;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.api.rest.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;

@EBean
public class RequestErrorHandler implements RestErrorHandler {

    @RootContext
    Context context;

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {

        if (RestConfig.DEBUG) {
            System.out.println("*****************************");
            System.out.println(e.getMessage());
            System.out.println("*****************************");
        }

        String[] mensagemArray = e.getMessage().split(" ");

        int responseCode = Integer.valueOf(mensagemArray[0]);

        try {
            RestUtils.outKeyErrorHandler(responseCode, context);
        } catch (Exception exception) {
            //nothing
            exception.printStackTrace();
        }

    }
}
