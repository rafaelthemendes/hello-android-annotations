package br.unifor.helloworldofannotationswithrest.rest.config;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.springframework.http.HttpRequest;

import br.unifor.helloworldofannotationswithrest.utils.Const;

public class RestUtils {

    private static final String TAG = "RestUtils";

    public static void debugRequest(String tag, HttpRequest request) {

        if (RestConfig.DEBUG) {
            Log.d(tag, "URL: " + request.getURI());
            Log.d(tag, "METHOD: " + request.getMethod().name());
            Log.d(tag, "HEADERS: ");
            for (String headerKey : request.getHeaders().keySet()) {
                Log.d(tag, " -> " + request.getHeaders().get(headerKey));
            }
        }
    }

    public static void outKeyErrorHandler(int responseCode, Context context)
            throws Exception {

        if (context == null) {
            return;
        }

        Intent intent = null;

        switch (responseCode) {

            case 401:
                // UNAUTHORIZED - 401
                intent = new Intent(Const.UNAUTHORIZED_REQUEST_ACTION);
                context.sendBroadcast(intent);
                throw new Exception("RestUtils: UNAUTHORIZED");

            case 226:
                // I'm used - 226 (spring4android)
                intent = new Intent(
                        Const.USUARIO_EXISTENTE_REQUEST_ACTION);
                context.sendBroadcast(intent);
                throw new Exception("RestUtils: USUARIO_EXISTENTE");

            case 409:
                // CONFLICT - 409 (spring4android)
                intent = new Intent(
                        Const.CONFLIT);
                context.sendBroadcast(intent);
                throw new Exception("RestUtils: CPF_EXISTENTE");

            case 406:
                // NOT ACCEPTABLE - 406 (spring4android)
                intent = new Intent(
                        Const.USER_PASSWORD_INVALID_REQUEST_ACTION);
                context.sendBroadcast(intent);

                throw new Exception("RestUtils: USUARIO_SENHA_INVALIDO");

            case 418:
                intent = new Intent(
                        Const.EMAIL_EXISTENTE_REQUEST_ACTION);
                context.sendBroadcast(intent);

                throw new Exception("RestUtils: EMAIL_EXISTENTE_REQUEST_ACTION");

            case 400:
                intent = new Intent(
                        Const.EMAIL_INVALIDO);
                context.sendBroadcast(intent);

                throw new Exception("RestUtils: EMAIL_INVALIDO");

            case 500:
                intent = new Intent(
                        Const.ERROR_SERVICO);
                context.sendBroadcast(intent);

                throw new Exception("RestUtils: ERROR_SERVICO");

            default:
                if (RestConfig.DEBUG)
                    Log.e(TAG, "responseCode = " + responseCode);

        }

    }

}
