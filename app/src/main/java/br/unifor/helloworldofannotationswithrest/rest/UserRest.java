package br.unifor.helloworldofannotationswithrest.rest;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.androidannotations.api.rest.RestClientHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import java.util.List;

import br.unifor.helloworldofannotationswithrest.model.Review;
import br.unifor.helloworldofannotationswithrest.model.User;
import br.unifor.helloworldofannotationswithrest.rest.config.BaseInterceptor;
import br.unifor.helloworldofannotationswithrest.rest.config.RestPath;
import br.unifor.helloworldofannotationswithrest.rest.entityRest.Login;
import br.unifor.helloworldofannotationswithrest.rest.entityRest.Register;

@Rest(rootUrl = RestPath.BASE_URL, converters = {MappingJacksonHttpMessageConverter.class, GsonHttpMessageConverter.class}, interceptors = {BaseInterceptor.class})
public interface UserRest extends RestClientErrorHandling, RestClientHeaders {

    @Post(RestPath.AUTH)
    @Accept(MediaType.APPLICATION_JSON)
    User login(Login login);


    @Post(RestPath.REGISTER)
    User register(Register register);

    /*
    @RequiresHeader(RestParam.PARAM_NAME_AUTHORIZATION)
    @Post(RestPath.PATH_DEVICES)
    ReviewEntity sendDeviceIdToService(ReviewEntity device);
    */

    @Get(RestPath.GET_FEED)
    List<Review> getFeed();
}