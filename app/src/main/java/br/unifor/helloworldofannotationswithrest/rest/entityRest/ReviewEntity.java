package br.unifor.helloworldofannotationswithrest.rest.entityRest;

/**
 * Created by marinagosson on 06/07/15.
 */
public class ReviewEntity {

    private Integer id;
    private String token;
    private Integer user;
    private String device_type;


    public ReviewEntity() {
    }

    public ReviewEntity(String token, Integer user, String device_type, Integer id) {
        this.token = token;
        this.user = user;
        this.device_type = device_type;
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
