package br.unifor.helloworldofannotationswithrest.rest.config;

public class RestPath {

    public static final String BASE_URL = "http://45.55.74.184:8080";

    public static final String VERSION = "";
    public static final String URL_APPLICATION = BASE_URL + VERSION;

    // Login
    public static final String AUTH = URL_APPLICATION + "/users/sign_in/";

    // Register
    public static final String REGISTER = URL_APPLICATION + "/users/sign_up/";

    // Reset Password
    public static final String RESET_PASSWORD = URL_APPLICATION + "/sign/request_reset_password/";

    // Get Feed
    public static final String GET_FEED = URL_APPLICATION + "/publications/list/";

    // Submit ReviewEntity
    public static final String SUBMIT_REVIEW = URL_APPLICATION + "/publications/post/";

    // Upload Image
    public static final String UPLOAD_IMAGE = "http://deway.com.br/treinamentos/unifor/upload.php";

}
