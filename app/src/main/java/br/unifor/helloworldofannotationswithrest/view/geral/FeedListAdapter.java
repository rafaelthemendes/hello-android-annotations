package br.unifor.helloworldofannotationswithrest.view.geral;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import br.unifor.helloworldofannotationswithrest.controller.UserController;
import br.unifor.helloworldofannotationswithrest.model.Review;

@EBean
public class FeedListAdapter extends BaseAdapter {

    List<Review> reviews;

    @Bean
    UserController userController;

    @RootContext
    Context context;

    @AfterInject
    public void afterInject() {
        reviews = userController.getReviews();
    }

    @Override
    public int getCount() {
        return reviews.size();
    }

    @Override
    public Review getItem(int position) {
        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ReviewItemView reviewItemView;
        if (convertView == null) {
            //reviewItemView = ReviewItemView_.build(context);
        } else {
            reviewItemView = (ReviewItemView) convertView;
        }

        //reviewItemView.bind(getItem(position));

        return null;//reviewItemView;

    }

    @Override
    public void notifyDataSetChanged() {
        afterInject();
    }
}
