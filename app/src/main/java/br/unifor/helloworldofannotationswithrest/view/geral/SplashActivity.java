package br.unifor.helloworldofannotationswithrest.view.geral;

import android.content.Intent;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.view.comom.BaseActivity;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";

    @AfterViews
    public void prepareActivity() {
        Log.i(TAG, "AfterViews");
        startTimer();
    }

    @Background(delay=2000)
    public void startTimer() {
        LoginActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

}
