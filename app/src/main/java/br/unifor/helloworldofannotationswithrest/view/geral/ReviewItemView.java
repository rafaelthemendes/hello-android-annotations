package br.unifor.helloworldofannotationswithrest.view.geral;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.model.Review;
import br.unifor.helloworldofannotationswithrest.utils.ImageViewRounded;
import br.unifor.helloworldofannotationswithrest.utils.Utils;

@EViewGroup(R.layout.item_review)
public class ReviewItemView extends LinearLayout {

    @ViewById(R.id.photo)
    ImageViewRounded photo;

    /*
    @ViewById(R.id.text_description_place)
    TextView description;
    */

    public ReviewItemView(Context context) {
        super(context);
    }

    public void bind(Review review) {
        // Carrega a foto do usuario
        UrlImageViewHelper.setUrlDrawable(photo,
                review.getPhoto(), R.drawable.user_photo);

        //description.setText(review.getComment());
    }
}