package br.unifor.helloworldofannotationswithrest.view.geral;

import android.widget.EditText;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.controller.UserController;
import br.unifor.helloworldofannotationswithrest.view.comom.BaseActivity;

@EActivity(R.layout.activity_signup)
public class SignUpActivity extends BaseActivity {

    @ViewById
    EditText editTextName;

    @ViewById
    EditText editTextEmail;

    @ViewById
    EditText editTextPassword;

    @ViewById
    EditText editTextConfirmPassword;

    @Bean
    UserController userController;

    @Click(R.id.buttonSignUp)
    void signUp(){
        String name = editTextName.getText().toString();
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        String confirmPassword = editTextConfirmPassword.getText().toString();

        if (!name.equalsIgnoreCase("") && !email.equalsIgnoreCase("")
                && !password.equalsIgnoreCase("") && !confirmPassword.equalsIgnoreCase("")) {

            if(password.equals(confirmPassword)) {
                startProgress();
                doSignUp();
            }else
                alertUser("Senha e Confirmação de senha não correspondem");

        } else
            alertUser("Preencha os campos corretamente.");
    }

    @Background
    void doSignUp() {
        try {
            userController.register(editTextName.getText().toString(), editTextEmail.getText().toString(), editTextPassword.getText().toString());
            stopProgress();
            //MenuPrincipalActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
            finish();
        } catch (Exception e) {
            stopProgress();
            e.printStackTrace();
        }
    }
}
