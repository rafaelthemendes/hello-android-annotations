package br.unifor.helloworldofannotationswithrest.view.geral;

import android.widget.EditText;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.view.comom.BaseActivity;

@EActivity(R.layout.activity_recover_password)
public class RecoverPasswordActivity extends BaseActivity {

    @ViewById
    EditText editTextEmail;

    @Click(R.id.buttonSendEmail)
    void recoverPassword(){

    }
}
