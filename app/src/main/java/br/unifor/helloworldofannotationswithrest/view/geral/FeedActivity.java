package br.unifor.helloworldofannotationswithrest.view.geral;

import android.widget.ListView;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.view.comom.BaseActivity;

@EActivity(R.layout.activity_feed)
public class FeedActivity extends BaseActivity {

    @ViewById
    ListView listViewFeed;

}
