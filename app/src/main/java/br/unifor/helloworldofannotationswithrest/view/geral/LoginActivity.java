package br.unifor.helloworldofannotationswithrest.view.geral;

import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.controller.UserController;
import br.unifor.helloworldofannotationswithrest.utils.Utils;
import br.unifor.helloworldofannotationswithrest.view.comom.BaseActivity;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {

    @ViewById
    EditText editTextEmail;

    @ViewById
    EditText editTextPassword;

    @ViewById
    TextView textViewForgotPassword;

    @ViewById
    TextView textViewSignUp;

    @Bean
    UserController userController;

    @AfterViews
    void setUnderlines(){
        textViewForgotPassword.setPaintFlags(textViewForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewSignUp.setPaintFlags(textViewSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Click(R.id.textViewForgotPassword)
    void goToPasswordRecovery(){
        RecoverPasswordActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
    }

    @Click(R.id.textViewSignUp)
    void goToSignUp(){
        SignUpActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
    }

    @Click(R.id.buttonLogin)
    public void login() {

        if (!editTextEmail.getText().toString().equalsIgnoreCase("")
                && !editTextPassword.getText().toString().equalsIgnoreCase("")) {

            startProgress();
            doLogin();

            /*
            OBS: Poderíamos checar se há conexão com a internet antes de fazer a requisição

            // verifica a conexao e alerta caso nao exista
            if (Utils.checkConnection(this)) {
                startProgress();
                doLogin();
            }

            */
        } else {
            alertUser("Preencha os campos corretamente.");
        }

    }

    @Background
    void doLogin(){
        try {
            userController.requestLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            stopProgress();
            //MenuPrincipalActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
            finish();
        } catch (Exception e) {
            stopProgress();
            e.printStackTrace();
        }
    }
}