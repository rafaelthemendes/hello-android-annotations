package br.unifor.helloworldofannotationswithrest.view.comom;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import br.unifor.helloworldofannotationswithrest.R;
import br.unifor.helloworldofannotationswithrest.utils.Const;
import br.unifor.helloworldofannotationswithrest.utils.Utils;

@EActivity
public class BaseActivity extends ActionBarActivity {

    private static final String TAG = "BaseAcitivity";

    protected ProgressDialog progressDialog;

    private static boolean alreadyLoggingOut = false;

    private boolean isActive = false;

    private boolean broadcastFired = false;

    protected BroadcastReceiver receiver;

    @AfterInject
    public void afterInject() {

        final Context context = this;

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String requestAction = intent.getAction();

                if (isActive) {

                    if (requestAction
                            .equalsIgnoreCase(Const.START_PROGRESS_REQUEST_ACTION)) {
                        try {
                            startProgress("Atualizando Informações", true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        broadcastFired = true;
                    }

                    if (requestAction
                            .equalsIgnoreCase(Const.STOP_PROGRESS_REQUEST_ACTION)) {
                        try {
                            stopProgress();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        broadcastFired = true;
                    }

                    if (requestAction
                            .equalsIgnoreCase(Const.UNAUTHORIZED_REQUEST_ACTION)) {
                        alertUser("Email ou senha inválidos");
                        broadcastFired = true;
                    }

                    if (requestAction.equalsIgnoreCase(Const.OTHERS_ERROS)) {
                        alertUser("Houve um problema. Por favor tente mais tarde.");
                        broadcastFired = true;
                    }

                    if (requestAction.equalsIgnoreCase(Const.EMAIL_INVALIDO)) {
                        alertUser("Email não encontrado");
                        broadcastFired = true;
                    }

                    if (requestAction.equalsIgnoreCase(Const.CONFLIT)) {
                        alertUser("Houve um problema. Por favor tente mais tarde.");
                        broadcastFired = true;
                    }

                    if (requestAction.equalsIgnoreCase(Const.ERROR_SERVICO)) {
                        alertUser("Por favor, entre em contato com o suporte técnico.");
                        broadcastFired = true;
                    }

                }

            }

        };

        IntentFilter filterStartProgress = new IntentFilter(Const.START_PROGRESS_REQUEST_ACTION);
        IntentFilter filterStopProgress = new IntentFilter(Const.STOP_PROGRESS_REQUEST_ACTION);
        IntentFilter filterRequestUnauthorized = new IntentFilter(Const.UNAUTHORIZED_REQUEST_ACTION);
        IntentFilter filterRequestBadRequest = new IntentFilter(Const.EMAIL_INVALIDO);
        IntentFilter filterUserOrPinInvalid = new IntentFilter(
                Const.USER_PASSWORD_INVALID_REQUEST_ACTION);
        IntentFilter filterOtherErros = new IntentFilter(
                Const.OTHERS_ERROS);
        IntentFilter filterErrosService = new IntentFilter(
                Const.CONFLIT);
        IntentFilter filterErrorService = new IntentFilter(
                Const.ERROR_SERVICO);

        this.registerReceiver(receiver, filterStartProgress);
        this.registerReceiver(receiver, filterStopProgress);
        this.registerReceiver(receiver, filterRequestUnauthorized);
        this.registerReceiver(receiver, filterUserOrPinInvalid);
        this.registerReceiver(receiver, filterOtherErros);
        this.registerReceiver(receiver, filterRequestBadRequest);
        this.registerReceiver(receiver, filterErrosService);
        this.registerReceiver(receiver, filterErrorService);


    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        // deixar a activity com a luz acessa direto
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setOrientation();
        this.isActive = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // generic dialog
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setTitle(getString(R.string.app_name));
        this.progressDialog.setMessage("Aguarde");
        this.progressDialog.setIcon(R.mipmap.ic_launcher);
        this.progressDialog.setCancelable(false);
        this.isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.isActive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Background
    public void forcaLogoutComErro() {

        if (alreadyLoggingOut == false) {
            alreadyLoggingOut = false;
        }
    }

    @UiThread
    protected void startProgress() {
        progressDialog.show();
    }

    @UiThread
    protected void startProgress(String mensagem, boolean cancelable) {
        try {
            if (cancelable) {
                progressDialog.setCancelable(cancelable);
            }

            progressDialog.setMessage(mensagem);

            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @UiThread
    protected void stopProgress() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setOrientation() {
        int current = getRequestedOrientation();
        if (current != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @UiThread
    public void alertUser(String message) {
        Utils.alertUser(message, this);
    }

    @UiThread
    public void alertUser(String message, OnClickListener positiveButton) {
        Utils.alertUser(message, this, positiveButton, null);
    }

    public static void logout() {
        //TODO:
    }

}