package br.unifor.helloworldofannotationswithrest.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties({"user"})
@DatabaseTable(tableName = "review")
public class Review {

    @DatabaseField(columnName = "id", id = true)
    private Integer id;

    @DatabaseField(columnName = "photo")
    private String photo;

    @DatabaseField(columnName = "comment")
    private String comment;

    @DatabaseField(columnName = "model")
    private String model;

    @DatabaseField(columnName = "brand")
    private String brand;

    @DatabaseField(columnName = "rating")
    private Integer rating;

    public Review(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
    
}
