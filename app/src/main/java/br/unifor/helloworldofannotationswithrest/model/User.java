package br.unifor.helloworldofannotationswithrest.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"success", "message"})
@DatabaseTable(tableName = "usuario")
public class User {

    @DatabaseField(columnName = "user_id", id = true)
    private Integer user_id;

    @DatabaseField(columnName = "username")
    private String username;

    @DatabaseField(columnName = "email")
    private String email;

    @DatabaseField(columnName = "token")
    private String token;

    public User() { }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
